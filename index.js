const express = require('express');
const path = require('path');

// init
const app = express();

// routes
app.get('/', (req, res) => {
    res.sendFile(path.join(__dirname, 'index.html'));
});

app.get('/about', (req, res) => {
    res.sendFile(path.join(__dirname, 'static/about.html'))
});

app.get('/contact', (req, res) => {
    res.sendFile(path.join(__dirname, 'static/contact.html'))
})

// app.get('/style.css', (req, res) => {
//     res.sendFile(path.join(__dirname, 'static/style.css'))
// })

app.use(express.static('public'))

app.listen(3000);
console.log('Server on port 3000')